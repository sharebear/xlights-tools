use std::fs::File;
use xlights_parsers::parse_with_reader;

fn main() {
    let scale = 0.006; // Pixel size in xml is defined as 2 units, 1 pixel is 12mm in real life.
    let file = File::open("/home/jshare/Documents/xLights/upstairsTree/xlights_rgbeffects.xml").unwrap();
    let result = parse_with_reader(file).unwrap();

    for model in result.iter() {
        println!(r#"<a-entity id="{}" position="{} {} {}" scale="{} {} {}" rotation="{} {} {}">"#,
                 model.name,
                 model.x * scale, model.y * scale, model.z * scale,
                 model.scale_x, model.scale_y, model.scale_z,
                 model.rotate_x, model.rotate_y, model.rotate_z
        );

        for pixel in &model.pixels {
            println!(r#"  <a-entity position="{} {} {}" geometry="primitive: sphere; radius: 0.012" material="color: #FF0000; shader: flat" light="type: point; intensity:10"></a-entity>"#, pixel.x * scale, pixel.y * scale, pixel.z * scale);
        }

        println!("</a-entity>");
    }
}
