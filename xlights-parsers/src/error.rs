use std::num::ParseIntError;

#[derive(Debug, PartialEq)]
pub enum Error {
    XmlParseError,
    XmlInvalidNumber(&'static str, String, ParseIntError)
}