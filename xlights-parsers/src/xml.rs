use std::io::Read;
use crate::error::Error;

#[derive(Debug, Deserialize, PartialEq)]
#[serde(rename_all = "PascalCase")]
pub struct Model {
    pub string_type: String,
    pub antialias: String,
    pub pixel_size: String,
    pub transparency: String,
    #[serde(rename = "parm1")]
    pub parm_1: String,
    #[serde(rename = "parm2")]
    pub parm_2: String,
    #[serde(rename = "parm3")]
    pub parm_3: String,
    pub layout_group: String,
    pub display_as: String,
    pub tree_bottom_top_ratio: String,
    pub tree_spiral_rotations: String,
    #[serde(rename = "name")]
    pub name: String,
    pub controller: String,
    pub dir: String,
    pub start_side: String,
    pub world_pos_x: String,
    pub world_pos_y: String,
    pub world_pos_z: String,
    pub scale_x: String,
    pub scale_y: String,
    pub scale_z: String,
    pub rotate_x: String,
    pub rotate_y: String,
    pub rotate_z: String,
    #[serde(rename = "versionNumber")]
    pub version_number: String,
    pub start_channel: String,
}

#[derive(Debug, Deserialize, PartialEq)]
pub struct Models {
    pub model: Vec<Model>
}

#[derive(Debug, Deserialize, PartialEq)]
pub struct Xrgb {
    pub models: Models
}

pub fn from_reader<R: Read>(reader: R) -> Result<Xrgb, Error> {
    serde_xml_rs::from_reader(reader)
        .map_err(|_err| Error::XmlParseError)
}