use crate::xml::Model;
use std::convert::TryFrom;
use crate::error::Error;
use std::num::ParseIntError;

const CHANNELS_PER_NODE: u16 = 3;

#[derive(Debug, PartialEq)]
struct Node {
    buf_x: usize,
    buf_y: usize,
    start_channel: u16,
}

#[derive(Debug, PartialEq)]
struct Strand {
    nodes: Vec<Node>
}

#[derive(Debug, PartialEq)]
struct LightString {
    strands: Vec<Strand>
}

#[derive(Debug, PartialEq)]
enum StartingLocation {
    TopLeft,
    BottomLeft,
    TopRight,
    BottomRight,
}

impl StartingLocation {
    fn starts_at_bottom(&self) -> bool {
        match self {
            StartingLocation::TopLeft => false,
            StartingLocation::BottomLeft => true,
            StartingLocation::TopRight => false,
            StartingLocation::BottomRight => true,
        }
    }

    fn starts_on_left(&self) -> bool {
        match self {
            StartingLocation::TopLeft => true,
            StartingLocation::BottomLeft => true,
            StartingLocation::TopRight => false,
            StartingLocation::BottomRight => false,
        }
    }
}

impl TryFrom<&Model> for StartingLocation {
    type Error = Error;

    fn try_from(model: &Model) -> Result<Self, Self::Error> {
        // TODO: In the future I assume the dependant props will become optional so using TryFrom
        // already now to allow error handling of that.
        Ok(match &*model.start_side {
            "B" => match &*model.dir {
                "R" => StartingLocation::BottomRight,
                _ => StartingLocation::BottomLeft
            },
            _ => match &*model.dir {
                "R" => StartingLocation::TopRight,
                _ => StartingLocation::TopLeft
            }
        })
    }
}

#[derive(Debug, PartialEq)]
pub struct Tree360 {
    name: String,
    strings: Vec<LightString>,
}

trait XmlParseError<Input> {
    fn map_error_context(self, field_name: &'static str, value: &String) -> Result<Input, Error>;
}

impl<Input> XmlParseError<Input> for Result<Input, ParseIntError> {
    fn map_error_context(self, field_name: &'static str, value: &String) -> Result<Input, Error> {
        self.map_err(|err| Error::XmlInvalidNumber(field_name, value.clone(), err))
    }
}

impl TryFrom<Model> for Tree360 {
    type Error = Error;

    fn try_from(model: Model) -> Result<Self, Self::Error> {
        let num_strings = model.parm_1.parse::<u16>()
            .map_error_context("parm_1", &model.parm_1)?;
        let pixels_per_string = model.parm_2.parse::<u16>()
            .map_error_context("parm_2", &model.parm_2)?;
        let strands_per_string = model.parm_3.parse::<u16>()
            .map_error_context("parm_3", &model.parm_3)?;
        let nodes_per_strand = pixels_per_string / strands_per_string;
        let number_of_strands = num_strings * strands_per_string;

        let starting_location = StartingLocation::try_from(&model)?;

        let mut current_channel: u16 = 0;
        let mut current_strand: u16 = 0;

        let mut strings = vec![];

        for _x in 0..num_strings {
            let mut strands = vec![];
            for _y in 0..strands_per_string {
                let mut nodes = vec![];
                for z in 0..nodes_per_strand {
                    let buf_x = if starting_location.starts_on_left() { current_strand } else { number_of_strands - current_strand - 1 };
                    let buf_y = if starting_location.starts_at_bottom() == (current_strand % 2 == 0) { z } else { nodes_per_strand - z - 1 };

                    nodes.push(Node {
                        buf_x: buf_x.into(),
                        buf_y: buf_y.into(),
                        start_channel: current_channel,
                    });
                    current_channel += CHANNELS_PER_NODE
                }
                strands.push(Strand { nodes });
                current_strand += 1;
            }
            strings.push(LightString { strands });
        }

        Ok(Tree360 {
            name: model.name,
            strings,
        })
    }
}

#[cfg(test)]
mod tests {
    use crate::xml::Model;
    use crate::model::tree_360::{Tree360, LightString, Strand, Node, StartingLocation};
    use crate::error::Error;
    use std::convert::TryFrom;

    #[test]
    fn should_err_if_parm_1_nan() {
        let input = ModelBuilder::default().with_parm_1("".to_string()).build();

        let result = Tree360::try_from(input);

        match result {
            Ok(_) => panic!("Test was expected to fail"),
            Err(error) => {
                match error {
                    Error::XmlParseError => panic!("Incorrect error received"),
                    Error::XmlInvalidNumber(field_name, _value, _original_err) => {
                        assert_eq!(field_name, "parm_1")
                    }
                }
            }
        }
    }

    #[test]
    fn should_create_strings_with_correct_nodes() {
        let input = ModelBuilder::default()
            .with_parm_1("2".to_string())
            .with_parm_2("4".to_string())
            .with_parm_3("2".to_string())
            .build();

        let result = Tree360::try_from(input).unwrap();

        assert_eq!(result, Tree360 {
            name: "TestModel".to_string(),
            strings: vec![
                LightString {
                    strands: vec![
                        Strand {
                            nodes: vec![
                                Node { buf_x: 0, buf_y: 0, start_channel: 0 },
                                Node { buf_x: 0, buf_y: 1, start_channel: 3 }
                            ]
                        },
                        Strand {
                            nodes: vec![
                                Node { buf_x: 1, buf_y: 1, start_channel: 6 },
                                Node { buf_x: 1, buf_y: 0, start_channel: 9 }
                            ]
                        }
                    ]
                },
                LightString {
                    strands: vec![
                        Strand {
                            nodes: vec![
                                Node { buf_x: 2, buf_y: 0, start_channel: 12 },
                                Node { buf_x: 2, buf_y: 1, start_channel: 15 }
                            ]
                        },
                        Strand {
                            nodes: vec![
                                Node { buf_x: 3, buf_y: 1, start_channel: 18 },
                                Node { buf_x: 3, buf_y: 0, start_channel: 21 }
                            ]
                        }
                    ]
                }
            ],
        })
    }

    #[test]
    fn should_map_start_pos_bottom_left() {
        let input = ModelBuilder::default()
            .with_dir("L".into())
            .with_start_side("B".into())
            .build();

        let result = StartingLocation::try_from(&input).unwrap();

        assert_eq!(result, StartingLocation::BottomLeft);
    }

    #[test]
    fn should_map_start_pos_bottom_right() {
        let input = ModelBuilder::default()
            .with_dir("R".into())
            .with_start_side("B".into())
            .build();

        let result = StartingLocation::try_from(&input).unwrap();

        assert_eq!(result, StartingLocation::BottomRight);
    }

    #[test]
    fn should_map_start_pos_top_left() {
        let input = ModelBuilder::default()
            .with_dir("L".into())
            .with_start_side("T".into())
            .build();

        let result = StartingLocation::try_from(&input).unwrap();

        assert_eq!(result, StartingLocation::TopLeft);
    }

    #[test]
    fn should_map_start_pos_top_right() {
        let input = ModelBuilder::default()
            .with_dir("R".into())
            .with_start_side("T".into())
            .build();

        let result = StartingLocation::try_from(&input).unwrap();

        assert_eq!(result, StartingLocation::TopRight);
    }

    struct ModelBuilder {
        parm_1: String,
        parm_2: String,
        parm_3: String,
        dir: String,
        start_side: String,
    }

    impl ModelBuilder {
        fn default() -> ModelBuilder {
            ModelBuilder {
                parm_1: "42".to_string(),
                parm_2: "42".to_string(),
                parm_3: "42".to_string(),
                dir: "L".to_string(),
                start_side: "B".to_string(),
            }
        }

        fn with_parm_1(&mut self, value: String) -> &mut ModelBuilder {
            self.parm_1 = value;
            self
        }
        fn with_parm_2(&mut self, value: String) -> &mut ModelBuilder {
            self.parm_2 = value;
            self
        }
        fn with_parm_3(&mut self, value: String) -> &mut ModelBuilder {
            self.parm_3 = value;
            self
        }
        fn with_dir(&mut self, value: String) -> &mut ModelBuilder {
            self.dir = value;
            self
        }
        fn with_start_side(&mut self, value: String) -> &mut ModelBuilder {
            self.start_side = value;
            self
        }


        fn build(&self) -> Model {
            Model {
                string_type: "".to_string(),
                antialias: "".to_string(),
                pixel_size: "".to_string(),
                transparency: "".to_string(),
                parm_1: self.parm_1.clone(),
                parm_2: self.parm_2.clone(),
                parm_3: self.parm_3.clone(),
                layout_group: "".to_string(),
                display_as: "".to_string(),
                tree_bottom_top_ratio: "".to_string(),
                tree_spiral_rotations: "".to_string(),
                name: "TestModel".to_string(),
                controller: "".to_string(),
                dir: self.dir.clone(),
                start_side: self.start_side.clone(),
                world_pos_x: "".to_string(),
                world_pos_y: "".to_string(),
                world_pos_z: "".to_string(),
                scale_x: "".to_string(),
                scale_y: "".to_string(),
                scale_z: "".to_string(),
                rotate_x: "".to_string(),
                rotate_y: "".to_string(),
                rotate_z: "".to_string(),
                version_number: "".to_string(),
                start_channel: "".to_string(),
            }
        }
    }
}