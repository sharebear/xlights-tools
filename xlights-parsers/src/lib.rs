mod xml;
mod error;
mod model;

extern crate serde;
extern crate serde_xml_rs;

#[macro_use]
extern crate serde_derive;

use crate::ConvertionErrors::ModelTypeNotSupported;
use std::io::Read;
use serde_xml_rs::from_reader;
use xml::{Model, Xrgb};

// FIXME: Should we model as a list of strands/strings to process more functionally?
#[derive(Debug, PartialEq)]
struct Tree360Data {
    num_strings: u16,
    strands_per_string: u16,
    pixels_per_string: u16,
    num_strands: u16,
    pixels_per_strand: u16,
    bot_top_ratio: f64,
    spiral_rotations: f64,
    // FIXME: I would prefer enum to bool
    is_bot_to_top: bool,
    // FIXME: I would prefer enum to bool
    is_l_to_r: bool,
}

#[derive(Debug, PartialEq)]
enum ModelType {
    Tree180,
    Tree360(Tree360Data),
    TreeRibbon,
    TreeFlat,
}

#[derive(Debug, PartialEq)]
pub enum ConvertionErrors {
    ModelTypeNotSupported,
    XmlParseError,
}

fn to_model_type(input: &Model) -> Result<ModelType, ConvertionErrors> {
    match input.display_as.as_str() {
        "Tree 360" => Ok(ModelType::Tree360(tree_360_data_from_model(input))),
        _ => Err(ModelTypeNotSupported),
    }
}

fn tree_360_data_from_model(input: &Model) -> Tree360Data {
    // TODO: This method is currently missing defaulting functionality

    let num_strings = input.parm_1.parse::<u16>().unwrap();
    let pixels_per_string = input.parm_2.parse::<u16>().unwrap();
    let strands_per_string = input.parm_3.parse::<u16>().unwrap();

    Tree360Data {
        num_strings,
        pixels_per_string,
        strands_per_string,
        num_strands: num_strings * strands_per_string,
        pixels_per_strand: pixels_per_string / strands_per_string,
        bot_top_ratio: input.tree_bottom_top_ratio.parse().unwrap(),
        spiral_rotations: input.tree_spiral_rotations.parse().unwrap(),
        is_bot_to_top: input.start_side == "B",
        is_l_to_r: input.dir != "R",
    }
}

#[derive(Debug, PartialEq)]
pub struct PubModel {
    pub name: String,
    pub x: f64,
    pub y: f64,
    pub z: f64,
    pub scale_x: f64,
    pub scale_y: f64,
    pub scale_z: f64,
    pub rotate_x: f64,
    pub rotate_y: f64,
    pub rotate_z: f64,
    pub pixels: Vec<Pixel>,
}

#[derive(Debug, PartialEq)]
pub struct Pixel {
    pub x: f64,
    pub y: f64,
    pub z: f64,
    pub start_channel: u16,
}

fn generate_pixels(input: ModelType) -> Result<Vec<Pixel>, ConvertionErrors> {
    match input {
        ModelType::Tree360(data) => Ok(generate_pixels_tree360(data)),
        _ => Err(ModelTypeNotSupported)
    }
}

fn generate_pixels_tree360(input: Tree360Data) -> Vec<Pixel> {
    // FROM MatrixModel::InitVMatrix, but skipping single node logic and firstExportedStrand logic
    let strand_start_chan = get_start_channels(&input);

    // Temporarily using a tuple here, x, y, startchan, lets see how it goes
    let mut nodes = vec![(0, 0, 0); (input.num_strands + input.pixels_per_strand) as usize];
    for x in 0..input.num_strands as usize {
        let string_num = x as u16 / input.strands_per_string;
        let segment_num = x as u16 % input.strands_per_string;
        for y in 0..input.pixels_per_strand {
            let idx = string_num * input.pixels_per_string + segment_num * input.pixels_per_strand + y;
            let buf_x = if input.is_l_to_r { x as u16 } else { input.num_strands - x as u16 - 1 };
            let buf_y = if input.is_bot_to_top == (segment_num % 2 == 0) { y } else { input.pixels_per_strand - y - 1 };
            let start_chan = strand_start_chan[x] + y * 3;
            nodes[idx as usize] = (buf_x, buf_y, start_chan);
        }
    }

    // FROM TreeModel::SetTreeCoord
    let render_ht = input.pixels_per_strand * 3;
    let render_wi = render_ht as f64 / 1.8;
    let radians = (360 as f64).to_radians();// Alternatively PI * 2
    let (radius, top_radius) = calculate_radius(render_wi, input.bot_top_ratio);
    let start_angle = (-radians / 2.0) + 3.0_f64.to_radians(); // 3.0 comes from TreeRotation that defaults to 3 and isn't present in my XML... watch out for it later
    let angle_increment = radians / (input.num_strands as f64);

    let mut y_pos = vec![0.0; input.pixels_per_strand as usize];
    for x in 0..input.pixels_per_strand as usize {
        y_pos[x] = x as f64;
    }
    let mut x_inc = vec![0.0; input.pixels_per_strand as usize];

    if input.spiral_rotations != 0.0 {
        let lengths = get_segment_lengths(&input, radius, top_radius);

        let mut cur_seg = 0;
        let mut lights_in_seg = (lengths[0] * (input.pixels_per_strand as f64)).round() as i32;

        let mut cur_light_in_seg = 0;
        for x in 1..input.pixels_per_strand {
            if cur_light_in_seg >= lights_in_seg {
                cur_seg += 1;
                cur_light_in_seg = 0;
                if cur_seg == 9 {
                    lights_in_seg = (input.pixels_per_strand - x) as i32;
                } else {
                    lights_in_seg = (lengths[cur_seg] * input.pixels_per_strand as f64).round() as i32;
                }
            }
            let mut ang = input.spiral_rotations * 2.0 * std::f64::consts::PI / 10.0;
            ang /= lights_in_seg as f64;
            let index = x as usize;
            y_pos[index] = y_pos[index - 1] + (input.pixels_per_strand as f64 / 10.0 / lights_in_seg as f64);
            x_inc[index] = x_inc[index - 1] + ang;
            cur_light_in_seg += 1;
        }
    }

    let top_y_offset = 0.0;
    let y_top = render_ht as f64 - top_y_offset;
    let y_bot = 0.0;

    let node_count = input.pixels_per_strand * input.num_strands;
    let mut result = vec![];

    for n in 0..node_count as usize {
        let (buffer_x, buffer_y, start_channel) = nodes[n];
        let angle = start_angle + buffer_x as f64 * angle_increment + x_inc[buffer_y as usize];
        let xb = radius * angle.sin();
        let xt = top_radius * angle.sin();
        let zb = radius * angle.cos();
        let zt = top_radius * angle.cos();
        let yb = y_bot;
        let yt = y_top;
        let pos_on_string = y_pos[buffer_y as usize] / (input.pixels_per_strand as f64 - 1.0);

        result.push(Pixel {
            x: xb + (xt - xb) * pos_on_string,
            y: yb + (yt - yb) * pos_on_string - input.pixels_per_strand as f64 / 2.0,
            z: zb + (zt - zb) * pos_on_string,
            start_channel,
        })
    }

    return result;
}

fn get_segment_lengths(input: &Tree360Data, radius: f64, top_radius: f64) -> Vec<f64> {
    let mut lengths = vec![0.0; 10];
    let rgap = (radius - top_radius) / 10.0;
    let mut total = 0;
    for x in 0..10 {
        lengths[x] = 2.0 * std::f64::consts::PI * (radius - rgap * (x as f64)) - rgap / 2.0;
        lengths[x] *= input.spiral_rotations / 10.0;
        lengths[x] = (lengths[x] * lengths[x] + (input.pixels_per_strand as f64) / 10.0 * (input.pixels_per_strand as f64) / 10.0).sqrt();
        total += lengths[x] as i32;
    }

    for x in 0..10 {
        lengths[x] /= total as f64;
    }
    lengths
}

fn get_start_channels(input: &Tree360Data) -> Vec<u16> {
    let mut strand_start_chan = vec![0; input.num_strands as usize];
    for x in 0..input.num_strands as usize {
        let string_num = x as u16 / input.strands_per_string;
        let segment_num = x as u16 % input.strands_per_string;
        strand_start_chan[x] = strand_start_chan[string_num as usize] + segment_num * input.pixels_per_strand * 3; // Hardcoding 3 here as number of channels per RGB pixel
    }
    return strand_start_chan;
}

fn calculate_radius(render_wi: f64, bot_top_ratio: f64) -> (f64, f64) {
    let radius = render_wi / 2.0;
    if bot_top_ratio == 0.0 {
        return (radius, radius);
    }

    let other_radius = radius / bot_top_ratio.abs();
    if bot_top_ratio < 0.0 {
        return (other_radius, radius);
    }

    return (radius, other_radius);
}

pub fn parse_with_reader<R: Read>(reader: R) -> Result<Vec<PubModel>, ConvertionErrors> {
    return from_reader(reader)
        .map(|xrgb: Xrgb| {
            return xrgb.models.model.iter().map(|model| {
                PubModel {
                    name: model.name.clone(),
                    x: model.world_pos_x.parse().unwrap(),
                    y: model.world_pos_y.parse().unwrap(),
                    z: model.world_pos_z.parse().unwrap(),
                    scale_x: model.scale_x.parse().unwrap(),
                    scale_y: model.scale_y.parse().unwrap(),
                    scale_z: model.scale_z.parse().unwrap(),
                    rotate_x: model.rotate_x.parse().unwrap(),
                    rotate_y: model.rotate_y.parse().unwrap(),
                    rotate_z: model.rotate_z.parse().unwrap(),
                    pixels: generate_pixels(to_model_type(model).unwrap()).unwrap(),
                }
            }).collect();
        })
        .map_err(|_err| {
            ConvertionErrors::XmlParseError // FIXME: Add context, for now I just want a running PoC
        });
}

#[cfg(test)]
mod tests {
    use crate::{Xrgb, Model, to_model_type, ModelType, generate_pixels, Tree360Data, get_start_channels, get_segment_lengths, Pixel};
    use serde_xml_rs::from_str;
    use crate::xml::Models;

    #[test]
    fn test_segment_calculation() {
        let input = Tree360Data {
            num_strings: 1,
            strands_per_string: 1,
            pixels_per_string: 150,
            num_strands: 1,
            pixels_per_strand: 150,
            bot_top_ratio: 0.0,
            spiral_rotations: 8.0,
            is_bot_to_top: false,
            is_l_to_r: true,
        };
        let result = get_segment_lengths(&input, 125.0, 125.0);
        assert_eq!(result, vec![0.10007922842750583; 10])
    }

    #[test]
    fn test_start_channels() {
        let input = Tree360Data {
            num_strings: 1,
            strands_per_string: 1,
            pixels_per_string: 150,
            num_strands: 1,
            pixels_per_strand: 150,
            bot_top_ratio: 0.0,
            spiral_rotations: 8.0,
            is_bot_to_top: false,
            is_l_to_r: true,
        };

        let result = get_start_channels(&input);
        assert_eq!(result, vec![0])
    }

    #[test]
    fn get_mah_model() {
        let input = ModelType::Tree360(Tree360Data {
            num_strings: 1,
            strands_per_string: 1,
            pixels_per_string: 150,
            num_strands: 1,
            pixels_per_strand: 150,
            bot_top_ratio: 0.0,
            spiral_rotations: 8.0,
            is_bot_to_top: false,
            is_l_to_r: true,
        });

        let result = generate_pixels(input).unwrap();

        println!("{:#?}", result);

        assert_eq!(result, vec![
            Pixel {
                x: -6.541994530372665,
                y: 378.02013422818845,
                z: -124.8286918443215,
                start_channel: 0,
            },
            Pixel {
                x: 37.73681623423328,
                y: 374.7842761265585,
                z: -119.16766633824676,
                start_channel: 3,
            },
            Pixel {
                x: 77.20304106119852,
                y: 371.54841802492854,
                z: -98.30915751293414,
                start_channel: 6,
            },
            Pixel {
                x: 106.82354208761777,
                y: 368.3125599232986,
                z: -64.91325639540013,
                start_channel: 9,
            },
            Pixel {
                x: 122.82080915416016,
                y: 365.0767018216686,
                z: -23.23895089536894,
                start_channel: 12,
            },
            Pixel {
                x: 123.1547066530082,
                y: 361.8408437200387,
                z: 21.399024024742246,
                start_channel: 15,
            },
            Pixel {
                x: 107.7826525497477,
                y: 358.60498561840876,
                z: 63.3079758746113,
                start_channel: 18,
            },
            Pixel {
                x: 78.66504888123333,
                y: 355.3691275167788,
                z: 97.14324518211839,
                start_channel: 21,
            },
            Pixel {
                x: 39.5152711766325,
                y: 352.1332694151488,
                z: 118.58981129859848,
                start_channel: 24,
            },
            Pixel {
                x: -4.673899284535998,
                y: 348.8974113135189,
                z: 124.91258809854999,
                start_channel: 27,
            },
            Pixel {
                x: -48.26700616071394,
                y: 345.66155321188893,
                z: 115.30523022084297,
                start_channel: 30,
            },
            Pixel {
                x: -85.70460925355084,
                y: 342.42569511025897,
                z: 90.9929665012421,
                start_channel: 33,
            },
            Pixel {
                x: -112.21228135474084,
                y: 339.189837008629,
                z: 55.076346221989695,
                start_channel: 36,
            },
            Pixel {
                x: -124.40949218314034,
                y: 335.9539789069991,
                z: 12.135825259665708,
                start_channel: 39,
            },
            Pixel {
                x: -120.74072828613475,
                y: 332.71812080536915,
                z: -32.35238063781054,
                start_channel: 42,
            },
            Pixel {
                x: -103.38507178432285,
                y: 329.6979865771812,
                z: -70.26042223151246,
                start_channel: 45,
            },
            Pixel {
                x: -74.52810937070566,
                y: 326.6778523489933,
                z: -100.35218439888658,
                start_channel: 48,
            },
            Pixel {
                x: -37.38009903201517,
                y: 323.6577181208054,
                z: -119.28004106453325,
                start_channel: 51,
            },
            Pixel {
                x: 3.9263448847615523,
                y: 320.63758389261744,
                z: -124.9383200457166,
                start_channel: 54,
            },
            Pixel {
                x: 44.7959936931584,
                y: 317.6174496644295,
                z: -116.6975533121518,
                start_channel: 57,
            },
            Pixel {
                x: 80.68221096549048,
                y: 314.5973154362416,
                z: -95.4745035793331,
                start_channel: 60,
            },
            Pixel {
                x: 107.59275337549076,
                y: 311.5771812080537,
                z: -63.63017696880012,
                start_channel: 63,
            },
            Pixel {
                x: 122.53389682771439,
                y: 308.5570469798658,
                z: -24.707167547394942,
                start_channel: 66,
            },
            Pixel {
                x: 123.84348005893102,
                y: 305.53691275167785,
                z: 16.96444655428389,
                start_channel: 69,
            },
            Pixel {
                x: 111.37581552354786,
                y: 302.51677852348996,
                z: 56.748812467439656,
                start_channel: 72,
            },
            Pixel {
                x: 86.5178967338038,
                y: 299.496644295302,
                z: 90.22002851229246,
                start_channel: 75,
            },
            Pixel {
                x: 52.03509903255381,
                y: 296.47651006711413,
                z: 113.6545136308819,
                start_channel: 78,
            },
            Pixel {
                x: 11.76353916481824,
                y: 293.4563758389262,
                z: 124.44524557538462,
                start_channel: 81,
            },
            Pixel {
                x: -29.816682197318812,
                y: 290.43624161073825,
                z: 121.39178498870544,
                start_channel: 84,
            },
            Pixel {
                x: -68.07987937687516,
                y: 287.4161073825503,
                z: 104.8338209931801,
                start_channel: 87,
            },
            Pixel {
                x: -98.76937654695897,
                y: 284.3959731543624,
                z: 76.61338170662506,
                start_channel: 90,
            },
            Pixel {
                x: -118.47105125119701,
                y: 281.3758389261745,
                z: 39.869913662262285,
                start_channel: 93,
            },
            Pixel {
                x: -124.99314617068906,
                y: 278.35570469798654,
                z: -1.3089730145270442,
                start_channel: 96,
            },
            Pixel {
                x: -117.61009611927942,
                y: 275.33557046979865,
                z: -42.34224003065801,
                start_channel: 99,
            },
            Pixel {
                x: -97.1432451821236,
                y: 272.3154362416107,
                z: -78.66504888122691,
                start_channel: 102,
            },
            Pixel {
                x: -65.86947443708767,
                y: 269.2953020134228,
                z: -106.23658662335613,
                start_channel: 105,
            },
            Pixel {
                x: -27.267905174571187,
                y: 266.2751677852349,
                z: -121.98959524234266,
                start_channel: 108,
            },
            Pixel {
                x: 14.367143811604956,
                y: 263.255033557047,
                z: -124.17159570004995,
                start_channel: 111,
            },
            Pixel {
                x: 54.40388742153794,
                y: 260.23489932885906,
                z: -112.53984642527564,
                start_channel: 114,
            },
            Pixel {
                x: 88.38834764831613,
                y: 257.2147651006712,
                z: -88.38834764832076,
                start_channel: 117,
            },
            Pixel {
                x: 112.53984642527278,
                y: 254.19463087248323,
                z: -54.40388742154384,
                start_channel: 120,
            },
            Pixel {
                x: 124.1715957000492,
                y: 251.1744966442953,
                z: -14.367143811611468,
                start_channel: 123,
            },
            Pixel {
                x: 121.9895952423441,
                y: 248.15436241610735,
                z: 27.26790517456479,
                start_channel: 126,
            },
            Pixel {
                x: 106.2365866233596,
                y: 245.13422818791946,
                z: 65.8694744370821,
                start_channel: 129,
            },
            Pixel {
                x: 78.66504888123201,
                y: 242.11409395973152,
                z: 97.14324518211947,
                start_channel: 132,
            },
            Pixel {
                x: 42.3422400306646,
                y: 239.09395973154363,
                z: 117.61009611927705,
                start_channel: 135,
            },
            Pixel {
                x: 1.3089730145340421,
                y: 236.0738255033557,
                z: 124.99314617068897,
                start_channel: 138,
            },
            Pixel {
                x: -39.869913662255655,
                y: 233.0536912751678,
                z: 118.47105125119926,
                start_channel: 141,
            },
            Pixel {
                x: -76.61338170661953,
                y: 230.03355704697987,
                z: 98.76937654696326,
                start_channel: 144,
            },
            Pixel {
                x: -104.83382099317629,
                y: 227.01342281879198,
                z: 68.07987937688102,
                start_channel: 147,
            },
            Pixel {
                x: -121.39178498870378,
                y: 223.99328859060404,
                z: 29.81668219732561,
                start_channel: 150,
            },
            Pixel {
                x: -124.44524557538529,
                y: 220.97315436241615,
                z: -11.763539164811272,
                start_channel: 153,
            },
            Pixel {
                x: -113.65451363088481,
                y: 217.95302013422815,
                z: -52.03509903254744,
                start_channel: 156,
            },
            Pixel {
                x: -90.22002851229729,
                y: 214.93288590604027,
                z: -86.51789673379874,
                start_channel: 159,
            },
            Pixel {
                x: -56.74881246744629,
                y: 211.91275167785233,
                z: -111.37581552354449,
                start_channel: 162,
            },
            Pixel {
                x: -16.964446554291264,
                y: 208.89261744966444,
                z: -123.84348005893001,
                start_channel: 165,
            },
            Pixel {
                x: 24.707167547387645,
                y: 205.8724832214765,
                z: -122.53389682771586,
                start_channel: 168,
            },
            Pixel {
                x: 63.63017696879372,
                y: 202.85234899328862,
                z: -107.59275337549455,
                start_channel: 171,
            },
            Pixel {
                x: 95.4745035793283,
                y: 199.83221476510067,
                z: -80.68221096549615,
                start_channel: 174,
            },
            Pixel {
                x: 116.69755331214914,
                y: 196.81208053691273,
                z: -44.79599369316535,
                start_channel: 177,
            },
            Pixel {
                x: 124.93832004571635,
                y: 193.79194630872485,
                z: -3.9263448847689917,
                start_channel: 180,
            },
            Pixel {
                x: 119.28004106453548,
                y: 190.7718120805369,
                z: 37.38009903200807,
                start_channel: 183,
            },
            Pixel {
                x: 100.35218439889101,
                y: 187.75167785234902,
                z: 74.52810937069968,
                start_channel: 186,
            },
            Pixel {
                x: 70.26042223151863,
                y: 184.73154362416108,
                z: 103.38507178431865,
                start_channel: 189,
            },
            Pixel {
                x: 32.35238063781773,
                y: 181.71140939597313,
                z: 120.74072828613284,
                start_channel: 192,
            },
            Pixel {
                x: -9.1547746409513,
                y: 178.69127516778522,
                z: 124.66430965305746,
                start_channel: 195,
            },
            Pixel {
                x: -49.64348632934519,
                y: 175.6711409395973,
                z: 114.71932821049869,
                start_channel: 198,
            },
            Pixel {
                x: -84.60949621033072,
                y: 172.6510067114094,
                z: 92.01213588996852,
                start_channel: 201,
            },
            Pixel {
                x: -110.16293150812285,
                y: 169.63087248322148,
                z: 59.06884560863395,
                start_channel: 204,
            },
            Pixel {
                x: -123.46104257439183,
                y: 166.61073825503357,
                z: 19.554308130031266,
                start_channel: 207,
            },
            Pixel {
                x: -123.02445099615566,
                y: 163.59060402684565,
                z: -22.13559253994557,
                start_channel: 210,
            },
            Pixel {
                x: -108.90172639002482,
                y: 160.57046979865774,
                z: -61.36296920189058,
                start_channel: 213,
            },
            Pixel {
                x: -82.66398316545819,
                y: 157.55033557046983,
                z: -93.76388370380594,
                start_channel: 216,
            },
            Pixel {
                x: -47.230098352310435,
                y: 154.53020134228188,
                z: -115.7338231012485,
                start_channel: 219,
            },
            Pixel {
                x: -6.541994530370126,
                y: 151.51006711409397,
                z: -124.82869184432163,
                start_channel: 222,
            },
            Pixel {
                x: 34.873888254901644,
                y: 148.48993288590603,
                z: -120.03671070961846,
                start_channel: 225,
            },
            Pixel {
                x: 72.41014654283319,
                y: 145.46979865771812,
                z: -101.89097446607046,
                start_channel: 228,
            },
            Pixel {
                x: 101.89097446606814,
                y: 142.4496644295302,
                z: -72.41014654283647,
                start_channel: 231,
            },
            Pixel {
                x: 120.03671070961734,
                y: 139.42953020134226,
                z: -34.873888254905495,
                start_channel: 234,
            },
            Pixel {
                x: 124.82869184432182,
                y: 136.40939597315435,
                z: 6.54199453036612,
                start_channel: 237,
            },
            Pixel {
                x: 115.73382310125002,
                y: 133.38926174496643,
                z: 47.23009835230672,
                start_channel: 240,
            },
            Pixel {
                x: 93.7638837038086,
                y: 130.36912751677852,
                z: 82.66398316545518,
                start_channel: 243,
            },
            Pixel {
                x: 61.362969201894074,
                y: 127.3489932885906,
                z: 108.90172639002286,
                start_channel: 246,
            },
            Pixel {
                x: 22.135592539949517,
                y: 124.32885906040269,
                z: 123.02445099615495,
                start_channel: 249,
            },
            Pixel {
                x: -19.554308130027305,
                y: 121.30872483221475,
                z: 123.46104257439245,
                start_channel: 252,
            },
            Pixel {
                x: -59.06884560863041,
                y: 118.28859060402684,
                z: 110.16293150812474,
                start_channel: 255,
            },
            Pixel {
                x: -92.0121358899658,
                y: 115.26845637583892,
                z: 84.60949621033366,
                start_channel: 258,
            },
            Pixel {
                x: -114.71932821049708,
                y: 112.24832214765101,
                z: 49.64348632934886,
                start_channel: 261,
            },
            Pixel {
                x: -124.66430965305716,
                y: 109.2281879194631,
                z: 9.154774640955303,
                start_channel: 264,
            },
            Pixel {
                x: -120.74072828613387,
                y: 106.20805369127515,
                z: -32.35238063781385,
                start_channel: 267,
            },
            Pixel {
                x: -103.38507178432093,
                y: 103.18791946308724,
                z: -70.2604222315153,
                start_channel: 270,
            },
            Pixel {
                x: -74.52810937070292,
                y: 100.16778523489933,
                z: -100.35218439888861,
                start_channel: 273,
            },
            Pixel {
                x: -37.38009903201189,
                y: 97.14765100671141,
                z: -119.28004106453427,
                start_channel: 276,
            },
            Pixel {
                x: 3.926344884764981,
                y: 94.1275167785235,
                z: -124.93832004571648,
                start_channel: 279,
            },
            Pixel {
                x: 44.795993693161606,
                y: 91.10738255033559,
                z: -116.69755331215057,
                start_channel: 282,
            },
            Pixel {
                x: 80.68221096549311,
                y: 88.08724832214764,
                z: -95.47450357933087,
                start_channel: 285,
            },
            Pixel {
                x: 107.5927533754925,
                y: 85.06711409395973,
                z: -63.63017696879718,
                start_channel: 288,
            },
            Pixel {
                x: 122.53389682771507,
                y: 82.04697986577182,
                z: -24.707167547391578,
                start_channel: 291,
            },
            Pixel {
                x: 123.84348005893057,
                y: 79.0268456375839,
                z: 16.96444655428729,
                start_channel: 294,
            },
            Pixel {
                x: 111.37581552354631,
                y: 76.00671140939599,
                z: 56.74881246744272,
                start_channel: 297,
            },
            Pixel {
                x: 86.51789673380132,
                y: 72.98657718120808,
                z: 90.22002851229483,
                start_channel: 300,
            },
            Pixel {
                x: 52.03509903255068,
                y: 69.96644295302013,
                z: 113.65451363088332,
                start_channel: 303,
            },
            Pixel {
                x: 11.763539164815047,
                y: 66.94630872483222,
                z: 124.44524557538493,
                start_channel: 306,
            },
            Pixel {
                x: -29.816682197321928,
                y: 63.92617449664431,
                z: 121.39178498870467,
                start_channel: 309,
            },
            Pixel {
                x: -68.07987937687784,
                y: 60.906040268456366,
                z: 104.83382099317835,
                start_channel: 312,
            },
            Pixel {
                x: -98.76937654696094,
                y: 57.88590604026845,
                z: 76.61338170662252,
                start_channel: 315,
            },
            Pixel {
                x: -118.47105125119805,
                y: 54.86577181208054,
                z: 39.86991366225925,
                start_channel: 318,
            },
            Pixel {
                x: -124.99314617068902,
                y: 51.84563758389261,
                z: -1.3089730145302523,
                start_channel: 321,
            },
            Pixel {
                x: -117.61009611927832,
                y: 48.8255033557047,
                z: -42.34224003066103,
                start_channel: 324,
            },
            Pixel {
                x: -97.14324518212159,
                y: 45.805369127516784,
                z: -78.66504888122941,
                start_channel: 327,
            },
            Pixel {
                x: -65.86947443708495,
                y: 42.78523489932887,
                z: -106.23658662335784,
                start_channel: 330,
            },
            Pixel {
                x: -27.267905174568057,
                y: 39.76510067114094,
                z: -121.98959524234337,
                start_channel: 333,
            },
            Pixel {
                x: 14.367143811608145,
                y: 36.744966442953015,
                z: -124.17159570004956,
                start_channel: 336,
            },
            Pixel {
                x: 54.40388742154082,
                y: 33.7248322147651,
                z: -112.53984642527425,
                start_channel: 339,
            },
            Pixel {
                x: 88.38834764831839,
                y: 30.704697986577173,
                z: -88.38834764831849,
                start_channel: 342,
            },
            Pixel {
                x: 112.53984642527419,
                y: 27.68456375838926,
                z: -54.403887421540944,
                start_channel: 345,
            },
            Pixel {
                x: 124.17159570004955,
                y: 24.664429530201346,
                z: -14.36714381160828,
                start_channel: 348,
            },
            Pixel {
                x: 121.9895952423434,
                y: 21.64429530201342,
                z: 27.26790517456792,
                start_channel: 351,
            },
            Pixel {
                x: 106.2365866233579,
                y: 18.624161073825505,
                z: 65.86947443708483,
                start_channel: 354,
            },
            Pixel {
                x: 78.66504888122951,
                y: 15.604026845637577,
                z: 97.1432451821215,
                start_channel: 357,
            },
            Pixel {
                x: 42.342240030661166,
                y: 12.583892617449663,
                z: 117.61009611927828,
                start_channel: 360,
            },
            Pixel {
                x: 1.3089730145303902,
                y: 9.56375838926175,
                z: 124.99314617068902,
                start_channel: 363,
            },
            Pixel {
                x: -39.869913662259115,
                y: 6.543624161073822,
                z: 118.47105125119809,
                start_channel: 366,
            },
            Pixel {
                x: -76.61338170662242,
                y: 3.5234899328859086,
                z: 98.76937654696103,
                start_channel: 369,
            },
            Pixel {
                x: -104.83382099317828,
                y: 0.5033557046979951,
                z: 68.07987937687795,
                start_channel: 372,
            },
            Pixel {
                x: -121.39178498870464,
                y: -2.5167785234899327,
                z: 29.816682197322063,
                start_channel: 375,
            },
            Pixel {
                x: -124.44524557538494,
                y: -5.536912751677846,
                z: -11.76353916481491,
                start_channel: 378,
            },
            Pixel {
                x: -113.65451363088329,
                y: -8.557046979865774,
                z: -52.03509903255076,
                start_channel: 381,
            },
            Pixel {
                x: -90.22002851229487,
                y: -11.577181208053695,
                z: -86.51789673380127,
                start_channel: 384,
            },
            Pixel {
                x: -56.74881246744289,
                y: -14.597315436241608,
                z: -111.37581552354622,
                start_channel: 387,
            },
            Pixel {
                x: -16.96444655428759,
                y: -17.61744966442953,
                z: -123.84348005893052,
                start_channel: 390,
            },
            Pixel {
                x: 24.707167547391172,
                y: -20.63758389261745,
                z: -122.53389682771515,
                start_channel: 393,
            },
            Pixel {
                x: 63.63017696879672,
                y: -23.65771812080537,
                z: -107.59275337549278,
                start_channel: 396,
            },
            Pixel {
                x: 95.47450357933047,
                y: -26.67785234899329,
                z: -80.6822109654936,
                start_channel: 399,
            },
            Pixel {
                x: 116.69755331215032,
                y: -29.69798657718121,
                z: -44.795993693162295,
                start_channel: 402,
            },
            Pixel {
                x: 124.93832004571645,
                y: -32.718120805369125,
                z: -3.9263448847658404,
                start_channel: 405,
            },
            Pixel {
                x: 119.28004106453456,
                y: -35.738255033557046,
                z: 37.38009903201097,
                start_channel: 408,
            },
            Pixel {
                x: 100.35218439888926,
                y: -38.758389261744966,
                z: 74.52810937070205,
                start_channel: 411,
            },
            Pixel {
                x: 70.2604222315163,
                y: -41.77852348993289,
                z: 103.38507178432025,
                start_channel: 414,
            },
            Pixel {
                x: 32.35238063781506,
                y: -44.79865771812081,
                z: 120.74072828613355,
                start_channel: 417,
            },
            Pixel {
                x: -9.154774640954,
                y: -47.81879194630872,
                z: 124.66430965305726,
                start_channel: 420,
            },
            Pixel {
                x: -49.64348632934761,
                y: -50.83892617449665,
                z: 114.71932821049762,
                start_channel: 423,
            },
            Pixel {
                x: -84.60949621033262,
                y: -53.85906040268456,
                z: 92.01213588996676,
                start_channel: 426,
            },
            Pixel {
                x: -110.16293150812405,
                y: -56.87919463087248,
                z: 59.06884560863171,
                start_channel: 429,
            },
            Pixel {
                x: -123.46104257439222,
                y: -59.899328859060404,
                z: 19.554308130028836,
                start_channel: 432,
            },
            Pixel {
                x: -123.02445099615524,
                y: -62.919463087248324,
                z: -22.135592539947933,
                start_channel: 435,
            },
            Pixel {
                x: -108.90172639002368,
                y: -65.93959731543625,
                z: -61.362969201892604,
                start_channel: 438,
            },
            Pixel {
                x: -82.66398316545647,
                y: -68.95973154362416,
                z: -93.76388370380745,
                start_channel: 441,
            },
            Pixel {
                x: -47.23009835230839,
                y: -71.97986577181209,
                z: -115.73382310124934,
                start_channel: 444,
            },
            Pixel {
                x: -6.541994530367976,
                y: -75.0,
                z: -124.82869184432172,
                start_channel: 447,
            }, ])
    }

    #[test]
    fn parse_tree() {
        let input = Model {
            string_type: "RGB Nodes".to_string(),
            antialias: "1".to_string(),
            pixel_size: "2".to_string(),
            transparency: "0".to_string(),
            parm_1: "1".to_string(),
            parm_2: "150".to_string(),
            parm_3: "1".to_string(),
            layout_group: "Default".to_string(),
            display_as: "Tree 360".to_string(),
            tree_bottom_top_ratio: "0.000000".to_string(),
            tree_spiral_rotations: "8.000000".to_string(),
            name: "Bucket".to_string(),
            controller: "Tree".to_string(),
            dir: "L".to_string(),
            start_side: "T".to_string(),
            world_pos_x: "269.4493".to_string(),
            world_pos_y: "124.9574".to_string(),
            world_pos_z: "0.0000".to_string(),
            scale_x: "0.3324".to_string(),
            scale_y: "0.1687".to_string(),
            scale_z: "0.3324".to_string(),
            rotate_x: "0".to_string(),
            rotate_y: "0".to_string(),
            rotate_z: "0".to_string(),
            version_number: "3".to_string(),
            start_channel: "!Tree:151".to_string(),
        };

        let result = to_model_type(&input).unwrap();
        assert_eq!(result, ModelType::Tree360(Tree360Data {
            num_strings: 1,
            num_strands: 1,
            pixels_per_strand: 150,
            bot_top_ratio: 0.0,
            spiral_rotations: 8.0,
            is_bot_to_top: false,
            is_l_to_r: true,
            strands_per_string: 1,
            pixels_per_string: 150,
        }));
    }

    #[test]
    fn complete_file() {
        let xml_string = r#"<?xml version="1.0" encoding="UTF-8"?>
<xrgb>
  <models>
    <model StringType="RGB Nodes" Antialias="1" PixelSize="2" Transparency="0" parm3="1" LayoutGroup="Default" DisplayAs="Tree 360" parm1="1" parm2="150" TreeBottomTopRatio="0.000000" TreeSpiralRotations="8.000000" name="Bucket" Controller="Tree" Dir="L" StartSide="T" WorldPosX="269.4493" WorldPosY="124.9574" WorldPosZ="0.0000" ScaleX="0.3324" ScaleY="0.1687" ScaleZ="0.3324" RotateX="0" RotateY="0" RotateZ="0" versionNumber="3" StartChannel="!Tree:151">
      <ControllerConnection Protocol="WS2811" Port="2" brightness="50" colorOrder="GRB"/>
      <subModel name="TopRing" layout="horizontal" type="ranges" line0="1-20"/>
      <subModel name="XylTop" layout="horizontal" type="ranges" line0="41-60"/>
      <subModel name="XylMiddle" layout="horizontal" type="ranges" line0="61-80"/>
      <subModel name="XylBottom" layout="horizontal" type="ranges" line0="81-100"/>
    </model>
    <model StringType="RGB Nodes" Antialias="1" PixelSize="2" Transparency="0" parm2="50" parm3="1" name="Tree" LayoutGroup="Default" DisplayAs="Tree 360" parm1="1" TreeSpiralRotations="3.000000" Dir="L" StartSide="B" TreeBottomTopRatio="4.000000" Controller="Tree" WorldPosX="287.9518" WorldPosY="393.4666" WorldPosZ="0.0000" ScaleX="3.0149" ScaleY="3.3149" ScaleZ="3.0149" RotateX="0" RotateY="0" RotateZ="0" versionNumber="3" StartChannel="!Tree:1">
      <ControllerConnection Port="1" Protocol="WS2811" brightness="50"/>
      <subModel name="0x" layout="horizontal" type="ranges" line0="1-10"/>
      <subModel name="1x" layout="horizontal" type="ranges" line0="11-20"/>
      <subModel name="2x" layout="horizontal" type="ranges" line0="21-30"/>
      <subModel name="3x" layout="horizontal" type="ranges" line0="31-40"/>
      <subModel name="4x" layout="horizontal" type="ranges" line0="41-50"/>
    </model>
  </models>
  <view_objects>
    <view_object DisplayAs="Gridlines" LayoutGroup="Default" name="Gridlines" GridLineSpacing="50" GridWidth="2000.0" GridHeight="1000.0" Active="1" WorldPosX="0.0000" WorldPosY="0.0000" WorldPosZ="0.0000" ScaleX="1.0000" ScaleY="1.0000" ScaleZ="1.0000" RotateX="90" RotateY="0" RotateZ="0" versionNumber="3"/>
  </view_objects>
  <effects version="0006"/>
  <views/>
  <palettes/>
  <modelGroups>
    <modelGroup selected="0" name="All" LayoutGroup="Default" models="Bucket,Tree" GridSize="400" layout="minimalGrid">
      <ControllerConnection/>
    </modelGroup>
  </modelGroups>
  <layoutGroups/>
  <perspectives current="Default Perspective">
    <perspective name="Default Perspective" settings="layout2|name=ModelPreview;caption=Model Preview;state=6293500;dir=4;layer=1;row=0;pos=0;prop=100000;bestw=250;besth=250;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|name=HousePreview;caption=House Preview;state=6293500;dir=4;layer=1;row=0;pos=1;prop=100000;bestw=250;besth=250;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|name=EffectAssist;caption=Effect Assist;state=2099196;dir=4;layer=1;row=0;pos=2;prop=100000;bestw=250;besth=250;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|name=DisplayElements;caption=Display Elements;state=6293503;dir=4;layer=0;row=0;pos=0;prop=100000;bestw=600;besth=400;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=600;floath=443|name=Perspectives;caption=Perspectives;state=2099198;dir=4;layer=1;row=0;pos=0;prop=100000;bestw=276;besth=219;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|name=Effect;caption=Effect Settings;state=2099196;dir=4;layer=0;row=1;pos=0;prop=100000;bestw=806;besth=1175;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|name=SelectEffect;caption=Select Effects;state=2099198;dir=4;layer=1;row=0;pos=0;prop=100000;bestw=729;besth=391;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|name=EffectDropper;caption=Effects;state=2099198;dir=1;layer=0;row=0;pos=0;prop=100000;bestw=1395;besth=930;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|name=ValueCurveDropper;caption=Value Curves;state=2099198;dir=1;layer=0;row=0;pos=0;prop=100000;bestw=40;besth=40;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|name=ColourDropper;caption=Colours;state=2099198;dir=1;layer=0;row=0;pos=0;prop=100000;bestw=20;besth=20;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|name=Jukebox;caption=Jukebox;state=2099198;dir=1;layer=0;row=0;pos=0;prop=100000;bestw=100;besth=200;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|name=Color;caption=Color;state=2099196;dir=1;layer=0;row=0;pos=0;prop=100000;bestw=600;besth=518;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|name=LayerTiming;caption=Layer Blending;state=2099196;dir=1;layer=0;row=0;pos=1;prop=100000;bestw=541;besth=366;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|name=LayerSettings;caption=Layer Settings;state=2099196;dir=1;layer=0;row=0;pos=2;prop=100000;bestw=609;besth=671;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|name=SequenceVideo;caption=Sequence Video;state=2099199;dir=4;layer=0;row=0;pos=0;prop=100000;bestw=680;besth=349;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=0;floaty=0;floatw=688;floath=373|name=Main Sequencer;caption=Main Sequencer;state=768;dir=5;layer=0;row=0;pos=0;prop=100000;bestw=136;besth=85;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|dock_size(4,1,0)=252|dock_size(4,0,1)=349|dock_size(1,0,0)=105|dock_size(5,0,0)=138|" version="2.0"/>
  </perspectives>
  <settings>
    <backgroundImage value="/home/jshare/Documents/xLights/upstairsTree/background.jpg"/>
    <backgroundBrightness value="8"/>
    <previewWidth value="650"/>
    <previewHeight value="720"/>
  </settings>
  <colors>
    <EffectDefault Red="192" Green="192" Blue="192"/>
    <EffectSelected Red="204" Green="102" Blue="255"/>
    <EffectSelectedFixed Red="255" Green="128" Blue="0"/>
    <EffectSelectedLocked Red="200" Green="0" Blue="0"/>
    <GridDashes Red="255" Green="255" Blue="0"/>
    <Gridlines Red="40" Green="40" Blue="40"/>
    <LabelOutline Red="103" Green="103" Blue="103"/>
    <Labels Red="255" Green="255" Blue="204"/>
    <LayoutDashes Red="255" Green="255" Blue="0"/>
    <ModelDefault Red="211" Green="211" Blue="211"/>
    <ModelOverlap Red="255" Green="0" Blue="0"/>
    <ModelSelected Red="255" Green="255" Blue="0"/>
    <Phonemes Red="255" Green="181" Blue="218"/>
    <Phrases Red="153" Green="255" Blue="153"/>
    <ReferenceEffect Red="255" Green="0" Blue="255"/>
    <ReferenceEffectLocked Red="255" Green="0" Blue="0"/>
    <RowHeaderSelected Red="130" Green="178" Blue="207"/>
    <Timing1 Red="0" Green="255" Blue="255"/>
    <Timing2 Red="255" Green="0" Blue="0"/>
    <Timing3 Red="0" Green="255" Blue="0"/>
    <Timing4 Red="0" Green="0" Blue="255"/>
    <Timing5 Red="255" Green="255" Blue="0"/>
    <TimingDefault Red="255" Green="255" Blue="255"/>
    <Waveform Red="130" Green="178" Blue="207"/>
    <WaveformSelected Red="0" Green="0" Blue="200"/>
    <WaveformSelectedEffect Red="255" Green="128" Blue="0"/>
    <Words Red="255" Green="218" Blue="145"/>
  </colors>
  <Viewpoints/>
</xrgb>
"#;

        let xrgb: Xrgb = from_str(xml_string).unwrap();
        println!("{:?}", xrgb);
        assert_eq!(xrgb, Xrgb {
            models: Models {
                model: vec![
                    Model {
                        string_type: "RGB Nodes".to_string(),
                        antialias: "1".to_string(),
                        pixel_size: "2".to_string(),
                        transparency: "0".to_string(),
                        parm_1: "1".to_string(),
                        parm_2: "150".to_string(),
                        parm_3: "1".to_string(),
                        layout_group: "Default".to_string(),
                        display_as: "Tree 360".to_string(),
                        tree_bottom_top_ratio: "0.000000".to_string(),
                        tree_spiral_rotations: "8.000000".to_string(),
                        name: "Bucket".to_string(),
                        controller: "Tree".to_string(),
                        dir: "L".to_string(),
                        start_side: "T".to_string(),
                        world_pos_x: "269.4493".to_string(),
                        world_pos_y: "124.9574".to_string(),
                        world_pos_z: "0.0000".to_string(),
                        scale_x: "0.3324".to_string(),
                        scale_y: "0.1687".to_string(),
                        scale_z: "0.3324".to_string(),
                        rotate_x: "0".to_string(),
                        rotate_y: "0".to_string(),
                        rotate_z: "0".to_string(),
                        version_number: "3".to_string(),
                        start_channel: "!Tree:151".to_string(),
                    },
                    Model {
                        string_type: "RGB Nodes".to_string(),
                        antialias: "1".to_string(),
                        pixel_size: "2".to_string(),
                        transparency: "0".to_string(),
                        parm_1: "1".to_string(),
                        parm_2: "50".to_string(),
                        parm_3: "1".to_string(),
                        layout_group: "Default".to_string(),
                        display_as: "Tree 360".to_string(),
                        tree_bottom_top_ratio: "4.000000".to_string(),
                        tree_spiral_rotations: "3.000000".to_string(),
                        name: "Tree".to_string(),
                        controller: "Tree".to_string(),
                        dir: "L".to_string(),
                        start_side: "B".to_string(),
                        world_pos_x: "287.9518".to_string(),
                        world_pos_y: "393.4666".to_string(),
                        world_pos_z: "0.0000".to_string(),
                        scale_x: "3.0149".to_string(),
                        scale_y: "3.3149".to_string(),
                        scale_z: "3.0149".to_string(),
                        rotate_x: "0".to_string(),
                        rotate_y: "0".to_string(),
                        rotate_z: "0".to_string(),
                        version_number: "3".to_string(),
                        start_channel: "!Tree:1".to_string(),
                    }
                ]
            }
        });
    }
}
