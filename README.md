# xlights-tools

This repository will contain Rust libraries and tools for working with the file
formats used by [xLights][xLights]. My initial goal is to read the models
defined in the `xlights_rgbeffects.xml` file and render them on the Oculus
Quest 2, preferably using web technology such as [a-frame][a-frame], coupled
with my [fseq-parser][fseq-parser] this could create a fun little VR
experience.

## Constraints

* Only support for version 3 of model xml. If you have an older version you can
  upgrade by opening and saving show directory.
* Only support for "RGB Nodes". In order to keep scope manageable I will only
  support pure pixel models, for the foreseeable future.

## License

While my Rust code is unlikely to resemble the C++ from xLights in the end,
this is not going to be a clean room implementation, so it seems fair and
correct to propagate the GPLv3 license from xLights here.  

[xLights]: https://xlights.org/
[a-frame]: https://aframe.io/
[fseq-parser]: https://gitlab.com/sharebear/fseq_parser
